/**
 * Created by wirtaw on 12.11.16.
 */
$(document).ready(function () {

    $.getJSON('js/data/instagram.json', function( data ) {
        var list = $('#list-of-images2');

        $.each( data, function( key, val ) {
            if ('undefined' !== typeof val) {
                $('<a/>').attr('id',''+val.id+'-url').attr('target','_blank')
                    .attr('href','https://instagram.com'+val.url+'media/?size=m').html(''+val.full_name).appendTo(list);
                $('<br/>').appendTo(list);
            }
        });
    });
});
