$(document).ready(function () {
    $('img.lazy').Lazy();
    var $grid = $('.grid').masonry({
        // options
        itemSelector: '.grid-item',
        percentPosition: true,
        columnWidth: '.grid-sizer',
        gutter: 10
    });
    $.ajax({
        method: 'post',
        url: 'get.php',
        data: {}
    }).done(function( data ) {
        //console.log("Data: " + data + " ");
        if ('' !== data) {
            try {
                var values = JSON.parse(data);
            } catch(e) {
                console.log(' ',e);
            };

            //debugger;
        }
        //$('#answer').html(' '+data);
    });
    $.getJSON('js/data/instagram.json', function( data ) {

        var photos = [];
        var more = [];
        var tags = [];
        var tags_obj = [];
        $.each( data, function( key, val ) {
            if ('undefined' !== typeof val) {
                var load = val.show;
                var t = val.tag;
                if (true === load) {
                    photos.push(val);
                } else {
                    more.push(val);
                }
                if (0 < t.length) {
                    for (var i in t) {
                        if (-1 === tags.indexOf(t[i])) {
                            tags.push(t[i]);
                            tags_obj.push({id:t[i],count:1});
                        } else {
                            for (var j in tags_obj) {
                                if (t[i] === tags_obj[j].id) {
                                    tags_obj[j].count++;
                                }
                            }
                        }
                    }
                }
            }
        });
        loadPhotos(photos);
        var sort_by = function(field, reverse, primer){

            var key = primer ?
                function(x) {return primer(x[field])} :
                function(x) {return x[field]};

            reverse = !reverse ? 1 : -1;

            return function (a, b) {
                return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
            }
        };
        if (0 < tags.length) {
            var new_tags = tags_obj.sort(sort_by('count', true, parseInt));
            var select = $('#get-by-tag');
            $('<option/>').val('all').html('All').appendTo(select);
            for (var i in tags_obj) {
                $('<option/>').val(tags_obj[i].id).html(tags_obj[i].id + ' '+ tags_obj[i].count).appendTo(select);
            }
        }

    });

    $('#load-more').on('click',function() {
        var list = $('#list-of-images');
        var loaded = [];
        list.find('img').each(function(){
            var self = $(this);
            loaded.push(self.attr('alt'));
        });
        var number = 20;
        var photos = [];
        $.getJSON('js/data/instagram.json', function( data ) {
            var i = 0;
            $.each( data, function( key, val ) {
                if ('undefined' !== typeof val) {
                    if (-1 === loaded.indexOf(val.full_name)) {
                        if (i < number) {
                            photos.push(val);
                        }
                        i++;
                    }
                }
            });
            loadPhotos(photos);
        });

    });

    $('#get-by-tag').on('change',function() {
        var self = $(this);
        var value = self.val();
        var list = $('#list-of-images');
        var photos = [];
        if ('' !== value && 'all' !== value) {


            $.getJSON('js/data/instagram.json', function( data ) {
                var i = 0;
                $.each( data, function( key, val ) {
                    if ('undefined' !== typeof val) {
                        if (-1 < val.full_name.indexOf(value)) {
                            photos.push(val);
                        }
                    }
                });
                list.html('');
                loadPhotos(photos);
            });
        } else if ('' !== value && 'all' === value) {
            var number = 20;
            $.getJSON('js/data/instagram.json', function( data ) {
                var i = 0;
                $.each( data, function( key, val ) {
                    if ('undefined' !== typeof val) {
                        if (i < number) {
                            photos.push(val);
                        }
                        i++;
                    }
                });
                list.html('');
                loadPhotos(photos);
            });
        }
    });

    $( window ).on('resize', function(e) {
        //clearTimeout(time);
        /*time = setTimeout(function(){
            //$('#list-of-images').msrItems('refresh');
        }, 200);*/
    });

    function loadPhotos(photos) {
        if (0 < photos.length) {
            var list = $('#list-of-images');
            for (var i in photos) {
                if ('undefined' !== typeof photos[i].cdn_image && '' !== photos[i].cdn_image) {
                    var tags_photo = photos[i].tag;

                    $('<div/>').attr('id', photos[i].id).addClass('grid-item').appendTo(list);
                    var elem = $('#' + photos[i].id);
                    var url = 'https://instagram.com' + photos[i].url;
                    $('<a/>').attr('id', photos[i].id + '-url').attr('href', url)
                        .attr('target', '_blank').appendTo(elem);
                    var link = $('#' + photos[i].id + '-url');

                    $('<img>').attr('data-src', photos[i].cdn_image).attr('src', '').attr('data-respond', 'data-respond')
                        .addClass('lazy').attr('width', 320)
                        .attr('id', photos[i].id + '-img').attr('alt', '' + photos[i].full_name)
                        .appendTo(link);
                    $('#' + photos[i].id + '-img').Lazy();
                    $('<div/>').attr('id', photos[i].id + '-text').text('' + photos[i].full_name).appendTo(elem);
                    var text = $('#' + photos[i].id + '-text');
                    var title = photos[i].full_name;

                    if (0 < tags_photo.length) {
                        text.html('');
                        $('<h4/>').html(photos[i].title).appendTo(text);
                        if (-1 !== title.indexOf(photos[i].title)) {
                            title = title.replace(photos[i].title, '');
                        }
                        for (var j = 0; j < tags_photo.length; j++) {
                            var tagname = '#' + tags_photo[j];
                            while (-1 < title.indexOf(tagname)) {
                                title = title.replace(tagname, '');
                                $('<a/>').html(tagname).attr('id',photos[i].id + '-tag-'+tags_photo[j])
                                    .attr('data-tag',tags_photo[j]).attr('href','#'+ tags_photo[j])
                                    .addClass('photo_tag').appendTo(text);
                                addEvent(photos[i].id + '-tag-'+tags_photo[j],'click',selectByTag);
                            }
                        }
                        $('<div/>').addClass('clrfix').appendTo(text);
                        //text.html(title);
                    } else {
                        text.html('');
                        $('<h4/>').html(photos[i].title).appendTo(text);
                    }
                    $grid.masonry();
                }
            }
            if(19 > photos.length) {
                $('#load-more').hide();
            }
        } else {
            $('#load-more').hide();
        }
    }

    function selectByTag() {

        var self= $(this);
        var tag = self.html();
        $('#get-by-tag').val('none');
        //tag = tag.replace(/#/g,'');
        var list = $('#list-of-images');
        var elems = $('#list-of-images .grid-item');
        if (0 < list.length && 0 < elems.length) {
            var hide = true;

            list.find('.grid-item').each(function() {
                var self =$(this);
                hide = true;
                self.find('a.photo_tag').each(function() {
                    var self2 =$(this);
                    if (tag === self2.html()) {
                        hide = false;
                    }
                });

                if (true === hide) {
                    self.hide();
                }
            });
        }

    }

    function addEvent(title, evnt, funct) {
        var answer = null;
        if ('undefined' !== typeof title && 'undefined' !== typeof evnt
            && '' !== evnt && 'undefined' !== typeof funct) {
            var element = document.getElementById('' + title);
            var elements = document.getElementsByClassName(''+title);
            var test = document.getElementById('answer');
            var remove_test = false;
            if (!test || null === test || 'undefined' === typeof test) {
                var body = null;
                if ('undefined' !== typeof jQuery) {
                    jQuery(document).find('body').each(function () {
                        body = jQuery(this);
                    });
                    jQuery('<input/>').attr('type', 'hidden').attr('id', 'answer_test').appendTo(body);
                } else {
                    var btn = document.createElement("input");
                    btn.setAttribute('type', 'hidden');
                    btn.setAttribute('id', 'border_test');
                    body = document.getElementsByTagName('body');
                    body[0].appendChild(btn);
                }
                test = document.getElementById('border_test');
                remove_test = true;
            }
            if (null !== test && element && 'undefined' !== typeof element && 'function' === typeof funct ) {
                if ( test.addEventListener ) {
                    answer = true;
                    element.addEventListener(evnt, funct, false);
                } else if ( test.attachEvent ) {
                    answer = element.attachEvent('on' + evnt, funct);
                } else {
                    answer = true;
                    element.setAttribute('onclick', funct);
                }
            } else if (elements && elements.length > 0 && 'function' === typeof funct) {
                var element2 = elements[0];
                for (var i=0 ; i<elements.length ; i++) {
                    element2 = elements[i];

                    if (test.attachEvent) {
                        answer = true;
                        element2.attachEvent('on' + evnt, funct);
                    } else if (test.addEventListener) {
                        answer = element2.addEventListener(evnt,funct, false);
                    } else {
                        answer = true;
                        element2.setAttribute('onclick',funct);
                    }
                }
            }
            if (true === remove_test) {
                if ('undefined' !== typeof jQuery) {
                    jQuery('#border_test').remove();
                } else {
                    body = document.getElementsByTagName('body');
                    body[0].removeChild(test);
                }
            }
        }
        return answer;
    }

});


